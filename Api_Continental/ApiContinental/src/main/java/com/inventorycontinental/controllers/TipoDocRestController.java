package com.inventorycontinental.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.inventorycontinental.entities.TipoDoc;
import com.inventorycontinental.servicesInterface.ITipoDocServices;



@RestController
@RequestMapping ("/api")
@CrossOrigin(origins="*", methods= {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT,RequestMethod.DELETE})

public class TipoDocRestController {
	
	
	

	@Autowired
	
	private ITipoDocServices tipo_docServices;
	
	
	@GetMapping("/tipo_doc")
	public List<TipoDoc> getAll(){
		return tipo_docServices.findAll();
	}
	
	@PostMapping("/tipo_doc")
	public TipoDoc create(@RequestBody TipoDoc tipo_doc) {
		System.out.println(tipo_doc.toString());
		return tipo_docServices.create(tipo_doc);
	}

	@PutMapping("/tipo_doc/{id}")
	public TipoDoc update(@PathVariable long id, @RequestBody TipoDoc tipo_doc){
		System.out.println(id);
		System.out.println(tipo_doc);
		return this.tipo_docServices.update(id, tipo_doc);		
	}


}

