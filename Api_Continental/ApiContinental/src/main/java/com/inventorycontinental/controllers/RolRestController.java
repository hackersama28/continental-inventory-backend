package com.inventorycontinental.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.inventorycontinental.entities.Rol;
import com.inventorycontinental.servicesInterface.IRolServices;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins="*", methods= {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT,RequestMethod.DELETE})
public class RolRestController {
	
	@Autowired
	private IRolServices rolService;
	
	@GetMapping("/rol")
	public List<Rol> getAll(){
		return rolService.findAll();
	}
    @PostMapping("/rol")
    public Rol create(@RequestBody Rol rol) {
    	System.out.print(rol.toString());
    	return rolService.create(rol);
    }
    @PutMapping("/rol/{id}")
	public Rol update(@PathVariable int id,@RequestBody Rol rol ){
		System.out.println(id);
		System.out.println(rol);
		return this.rolService.update(id,rol);
	}
    @DeleteMapping("/rol/{id}")
    public void delete(@PathVariable int id) {
    	this.rolService.delete(id);
    }
}


