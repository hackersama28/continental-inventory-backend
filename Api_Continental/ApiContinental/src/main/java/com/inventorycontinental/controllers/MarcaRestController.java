package com.inventorycontinental.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.inventorycontinental.entities.Marca;
import com.inventorycontinental.servicesInterface.IMarcaServices;



@RestController
@RequestMapping ("/api")
@CrossOrigin(origins="*", methods= {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT,RequestMethod.DELETE})


public class MarcaRestController {
	
	@Autowired
	
	private IMarcaServices marcaServices;
	
	
	@GetMapping("/marca")
	public List<Marca> getAll(){
		return marcaServices.findAll();
	}
	
	@PostMapping("/marca")
	public Marca create(@RequestBody Marca marca) {
		System.out.println(marca.toString());
		return marcaServices.create(marca);
	}

	@PutMapping("/marca/{id}")
	public Marca update(@PathVariable long id, @RequestBody Marca marca){
		System.out.println(id);
		System.out.println(marca);
		return this.marcaServices.update(id, marca);		
	}

}

