package com.inventorycontinental.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.inventorycontinental.entities.Venta;
import com.inventorycontinental.servicesInterface.IVentaServices;


@RestController
@RequestMapping ("/api")
@CrossOrigin(origins="*", methods= {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT})

public class VentaRestController {
	
	@Autowired
	
		private IVentaServices ventaService;
	
	
			@GetMapping("/venta")
			public List<Venta> getAll(){
				return ventaService.findAll();
			}
			
			@PostMapping("/venta")
			public Venta create(@RequestBody Venta venta) {
				System.out.println(venta.toString());
				return ventaService.create(venta);
	        }
			
			@PutMapping("/venta/{id}")
			public Venta update(@PathVariable long id, @RequestBody Venta venta){
				System.out.println(id);
				System.out.println(venta);
				return this.ventaService.update(id, venta);		
			}
		

}

