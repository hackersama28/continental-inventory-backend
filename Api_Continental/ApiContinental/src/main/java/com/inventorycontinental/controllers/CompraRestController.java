package com.inventorycontinental.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.inventorycontinental.entities.Compra;
import com.inventorycontinental.servicesInterface.ICompraServices;

@RestController
@RequestMapping ("/api")
@CrossOrigin(origins="*", methods= {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT})


public class CompraRestController {
	
@Autowired
	
	private ICompraServices compraService;


		@GetMapping("/compra")
		public List<Compra> getAll(){
			return compraService.findAll();
		}
		
		@PostMapping("/compra")
		public Compra create(@RequestBody Compra compra) {
			System.out.println(compra.toString());
			return compraService.create(compra);
        }
		
		@PutMapping("/compra/{id}")
		public Compra update(@PathVariable long id, @RequestBody Compra compra){
			System.out.println(id);
			System.out.println(compra);
			return this.compraService.update(id, compra);		
		}

}

