package com.inventorycontinental.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.inventorycontinental.entities.Cliente;
import com.inventorycontinental.servicesInterface.IClienteServices;



@RestController
@RequestMapping ("/api")
@CrossOrigin(origins="*", methods= {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT})

public class ClienteRestController {
	
@Autowired
	
	private IClienteServices clienteService;


		@GetMapping("/cliente")
		public List<Cliente> getAll(){
			return clienteService.findAll();
		}
		
		@PostMapping("/cliente")
		public Cliente create(@RequestBody Cliente cliente) {
			System.out.println(cliente.toString());
			return clienteService.create(cliente);
        }
		
		@PutMapping("/cliente/{id}")
		public Cliente update(@PathVariable long id, @RequestBody Cliente cliente){
			System.out.println(id);
			System.out.println(cliente);
			return this.clienteService.update(id, cliente);		
		}

}

