package com.inventorycontinental.controllers;



import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.inventorycontinental.entities.Producto;
import com.inventorycontinental.servicesInterface.IProductoServices;

@RestController
@RequestMapping ("/api")
@CrossOrigin(origins="*", methods= {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT})
public class ProductoRestController {

	@Autowired
	
	private IProductoServices productoServices;
	
	
	
	@GetMapping("/producto")
	public List<Producto> getAll(){
		return productoServices.findAll();
	}
	
	@PostMapping("/producto")
	public Producto create(@RequestBody Producto producto) {
		System.out.println(producto.toString());
		return productoServices.create(producto);
	}

	@PutMapping("/producto/{id}")
	public Producto update(@PathVariable long id, @RequestBody Producto producto){
		System.out.println(id);
		System.out.println(producto);
		return this.productoServices.update(id, producto);		
	}
	
}

