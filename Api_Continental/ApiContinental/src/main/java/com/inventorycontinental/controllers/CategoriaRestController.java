package com.inventorycontinental.controllers;




import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.inventorycontinental.entities.Categoria;
import com.inventorycontinental.servicesInterface.ICategoriaServices;



@RestController
@RequestMapping ("/api")
@CrossOrigin(origins="*", methods= {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT})

public class CategoriaRestController {
	
	
	
	@Autowired
	
	private ICategoriaServices categoriaService;


		@GetMapping("/categoria")
		public List<Categoria> getAll(){
			return categoriaService.findAll();
		}
		
		@PostMapping("/categoria")
		public Categoria create(@RequestBody Categoria categoria) {
			System.out.println(categoria.toString());
			return categoriaService.create(categoria);
        }
		
		@PutMapping("/categoria/{id}")
		public Categoria update(@PathVariable long id, @RequestBody Categoria categoria){
			System.out.println(id);
			System.out.println(categoria);
			return this.categoriaService.update(id, categoria);		
		}


}

