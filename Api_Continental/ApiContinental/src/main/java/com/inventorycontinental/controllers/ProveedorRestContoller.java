package com.inventorycontinental.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.inventorycontinental.entities.Proveedor;
import com.inventorycontinental.servicesInterface.IProveedorServices;

@RestController
@RequestMapping ("/api")
@CrossOrigin(origins="*", methods= {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT})


public class ProveedorRestContoller {
	
	
@Autowired
	
	private IProveedorServices proveedorService;


		@GetMapping("/proveedor")
		public List<Proveedor> getAll(){
			return proveedorService.findAll();
		}
		
		@PostMapping("/proveedor")
		public Proveedor create(@RequestBody Proveedor proveedor) {
			System.out.println(proveedor.toString());
			return proveedorService.create(proveedor);
        }
		
		@PutMapping("/proveedor/{id}")
		public Proveedor update(@PathVariable long id, @RequestBody Proveedor proveedor){
			System.out.println(id);
			System.out.println(proveedor);
			return this.proveedorService.update(id, proveedor);		
		}

}

