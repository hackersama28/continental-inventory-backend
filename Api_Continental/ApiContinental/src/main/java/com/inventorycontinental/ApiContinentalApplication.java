package com.inventorycontinental;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiContinentalApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiContinentalApplication.class, args);
	}

}
