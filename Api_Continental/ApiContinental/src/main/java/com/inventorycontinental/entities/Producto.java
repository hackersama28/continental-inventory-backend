package com.inventorycontinental.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;


@Entity
public class Producto implements Serializable {
	
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_producto_pk", nullable=false)
	private long id;
	
	@Column(name="nombre_producto", nullable=false)
	private String NombreProducto;
	
	@ManyToOne
	@JoinColumn(name = "id_categoria_pk", nullable=false)
	private Categoria categoria;
	
	@ManyToOne
	@JoinColumn(name = "id_marca_pk", nullable=false)
	private Marca marca;

	@Column(name="precio_compra", nullable=false)
	private double PrecioCompra;
	
	@Column(name="precio_venta", nullable=false)
	private double PrecioVenta;
	
	@Column(name="estado", nullable=false)
	private boolean estado;
	
	 @ManyToMany(mappedBy = "producto", cascade = CascadeType.PERSIST)
	    private List<Venta> venta;
	 
	 @ManyToMany(mappedBy = "producto", cascade = CascadeType.PERSIST)
	    private List<Proveedor> proveedor;
	 
	 @ManyToMany(mappedBy = "producto", cascade = CascadeType.PERSIST)
	    private List<Compra> compra;
	
	public Producto(long id, String nombreProducto, Categoria categoria, Marca marca, double precioCompra,
			double precioVenta, boolean estado) {
		super();
		this.id = id;
		NombreProducto = nombreProducto;
		this.categoria = categoria;
		this.marca = marca;
		PrecioCompra = precioCompra;
		PrecioVenta = precioVenta;
		this.estado = estado;
	}


	public Producto() {
		super();
	}


	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public String getNombreProducto() {
		return NombreProducto;
	}


	public void setNombreProducto(String nombreProducto) {
		NombreProducto = nombreProducto;
	}


	public Categoria getCategoria() {
		return categoria;
	}


	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}


	public Marca getMarca() {
		return marca;
	}


	public void setMarca(Marca marca) {
		this.marca = marca;
	}


	public double getPrecioCompra() {
		return PrecioCompra;
	}


	public void setPrecioCompra(double precioCompra) {
		PrecioCompra = precioCompra;
	}


	public double getPrecioVenta() {
		return PrecioVenta;
	}


	public void setPrecioVenta(double precioVenta) {
		PrecioVenta = precioVenta;
	}


	public boolean isEstado() {
		return estado;
	}


	public void setEstado(boolean estado) {
		this.estado = estado;
	}


	
	@Override
	public String toString() {
		return "Producto [id=" + id + ", NombreProducto=" + NombreProducto + ", categoria=" + categoria + ", marca="
				+ marca + ", PrecioCompra=" + PrecioCompra + ", PrecioVenta=" + PrecioVenta + ", estado=" + estado
				+ "]";
	}

	
	
	
	
	
	

}

