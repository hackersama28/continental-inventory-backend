package com.inventorycontinental.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

@Entity
public class Venta implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_venta_pk", nullable=false)
	private long id;
	
	@Column(name="codigo_venta", nullable=false)
	private String CodigoVenta;
	
	@ManyToOne
	@JoinColumn(name = "id_cliente_pk", nullable=false)
	private Cliente cliente;
	
	@ManyToOne
	@JoinColumn(name = "id_usuario_pk", nullable=false)
	private Usuario usuario;
	
	@Column(name="monto_total", nullable=false)
	private double MontoTotal;
	
	@Column(name="fecha_venta", nullable=false)
	private Date FechaVenta;
	
	@ManyToMany(cascade = CascadeType.PERSIST)
    @JoinTable(name = "venta_producto", joinColumns = @JoinColumn(name = "id_venta_pk"), inverseJoinColumns = @JoinColumn(name = "id_producto_pk"))
    private List<Producto> producto;


public Venta(long id, String codigoVenta, Cliente cliente, double montoTotal, Date fechaVenta,
		List<Producto> producto) {
			super();
			this.id = id;
			CodigoVenta = codigoVenta;
			this.cliente = cliente;
			MontoTotal = montoTotal;
			FechaVenta = fechaVenta;
			this.producto = producto;
		}



		public Venta() {
			super();
		}



		public long getId() {
			return id;
		}



		public void setId(long id) {
			this.id = id;
		}



		public String getCodigoVenta() {
			return CodigoVenta;
		}



		public void setCodigoVenta(String codigoVenta) {
			CodigoVenta = codigoVenta;
		}



		public Cliente getCliente() {
			return cliente;
		}



		public void setCliente(Cliente cliente) {
			this.cliente = cliente;
		}



		public double getMontoTotal() {
			return MontoTotal;
		}



		public void setMontoTotal(double montoTotal) {
			MontoTotal = montoTotal;
		}



		public Date getFechaVenta() {
			return FechaVenta;
		}



		public void setFechaVenta(Date fechaVenta) {
			FechaVenta = fechaVenta;
		}



		public List<Producto> getProducto() {
			return producto;
		}



		public void setProducto(List<Producto> producto) {
			this.producto = producto;
		}



		@Override
		public String toString() {
			return "Venta [id=" + id + ", CodigoVenta=" + CodigoVenta + ", cliente=" + cliente + ", MontoTotal="
					+ MontoTotal + ", FechaVenta=" + FechaVenta + ", producto=" + producto + "]";
		}
		
		
	
  
  
  
  
  
  
  
  
  
  

}

