package com.inventorycontinental.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;


@Entity
public class Cliente implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_cliente_pk", nullable=false)
	private long id;
	
	@Column(name="nombre", nullable=false)
	private String nombre;
	
	@ManyToOne
	@JoinColumn(name = "id_tipo_doc_pk", nullable=false)
	private TipoDoc tipo_doc;
	
	@Column(name="Numero_Documento", nullable=false)
	private double NumeroDocumento;
	
	@Column(name="correo_electronico", nullable=false)
	private String CorreoElectronico;
	
	@Column(name="Numero_Telefono", nullable=false)
	private double NumeroTelefono;
	
	@Column(name="Direccion", nullable=false)
	private String Direccion;

	public Cliente() {
		super();
	}

	
	
	public Cliente(long id, String nombre, TipoDoc tipo_doc, double numeroDocumento, String correoElectronico,
			double numeroTelefono, String direccion) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.tipo_doc = tipo_doc;
		NumeroDocumento = numeroDocumento;
		CorreoElectronico = correoElectronico;
		NumeroTelefono = numeroTelefono;
		Direccion = direccion;
	}



	public long getId() {
		return id;
	}



	public void setId(long id) {
		this.id = id;
	}



	public String getNombre() {
		return nombre;
	}



	public void setNombre(String nombre) {
		this.nombre = nombre;
	}



	public TipoDoc getTipo_doc() {
		return tipo_doc;
	}



	public void setTipo_doc(TipoDoc tipo_doc) {
		this.tipo_doc = tipo_doc;
	}



	public double getNumeroDocumento() {
		return NumeroDocumento;
	}



	public void setNumeroDocumento(double numeroDocumento) {
		NumeroDocumento = numeroDocumento;
	}



	public String getCorreoElectronico() {
		return CorreoElectronico;
	}



	public void setCorreoElectronico(String correoElectronico) {
		CorreoElectronico = correoElectronico;
	}



	public double getNumeroTelefono() {
		return NumeroTelefono;
	}



	public void setNumeroTelefono(double numeroTelefono) {
		NumeroTelefono = numeroTelefono;
	}



	public String getDireccion() {
		return Direccion;
	}



	public void setDireccion(String direccion) {
		Direccion = direccion;
	}



	@Override
	public String toString() {
		return "Cliente [id=" + id + ", nombre=" + nombre + ", tipo_doc=" + tipo_doc + ", NumeroDocumento="
				+ NumeroDocumento + ", CorreoElectronico=" + CorreoElectronico + ", NumeroTelefono=" + NumeroTelefono
				+ ", Direccion=" + Direccion + "]";
	}
	
	
	
	
	
	
	

}

