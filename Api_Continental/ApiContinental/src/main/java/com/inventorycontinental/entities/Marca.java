package com.inventorycontinental.entities;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="marcas")
public class Marca implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_marca_pk", nullable=false)
	private long id;
	
	@Column(name="nombre", nullable=false)
	private String nombre;
	
	@Column(name="codigo_marca", nullable=false)
	private String CodigoMarca;
	
	
	
	public Marca(long id, String nombre, String codigoMarca) {
		super();
		this.id = id;
		this.nombre = nombre;
		CodigoMarca = codigoMarca;
	}
	public Marca() {
		super();
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getCodigoMarca() {
		return CodigoMarca;
	}
	public void setCodigoMarca(String codigoMarca) {
		CodigoMarca = codigoMarca;
	}
	
	
	@Override
	public String toString() {
		return "Marca [id=" + id + ", nombre=" + nombre + ", CodigoMarca=" + CodigoMarca + "]";
	}
	
	
	
	
	
	
	

}
