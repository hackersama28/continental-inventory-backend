package com.inventorycontinental.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class TipoDoc implements Serializable {
	
	
	
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_tipo_doc_pk", nullable=false)
	private long id;
	
	@Column(name="tipo", nullable=false)
	private String tipo;
	
	public TipoDoc() {
		super();
	}
	

	public TipoDoc(long id, String tipo) {
		super();
		this.id = id;
		this.tipo = tipo;
	}


	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public String getTipo() {
		return tipo;
	}


	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	
	@Override
	public String toString() {
		return "Tipo_doc [id=" + id + ", tipo=" + tipo + "]";
	}

	
	
	
	

}
