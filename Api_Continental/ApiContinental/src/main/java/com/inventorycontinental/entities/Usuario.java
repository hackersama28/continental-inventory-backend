package com.inventorycontinental.entities;


import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@NamedQueries(value= {
		@NamedQuery(name="Usuario.findByDocumentoyContraseña",query="SELECT u FROM Usuario u "
				+ "WHERE u.num_documento= :num_documento AND u.contraseña= :contraseña"),
})

@Entity
@Table(name="usuario")
public class Usuario implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_usuario_pk",unique= true, nullable=false)
	private long id;
	
	@Column(name="num_documento",unique=true, nullable=false)
	private int num_documento;
	
	@Column(name="nombre_completo",nullable=false)
	private String nombre_completo;
	
	@Column(name="contraseña",unique= true, nullable=false)
	private String contraseña;
	
	@Column(name="correo_electronico",unique= true, nullable=false)
	private String correo_electronico;
	
	@Column(name="telefono",nullable=false)
	private int telefono;
	
	@ManyToOne
	@JoinColumn(name="id_rol_pk",nullable=false)
	private Rol rol;
	

	public Usuario(long id, int num_documento, String nombre_completo, String contraseña, String correo_electronico,
			int telefono, Rol rol) {
		super();
		this.id = id;
		this.num_documento = num_documento;
		this.nombre_completo = nombre_completo;
		this.contraseña = contraseña;
		this.correo_electronico = correo_electronico;
		this.telefono = telefono;
		this.rol = rol;
	}

	public Usuario() {
		super();
		
		
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getNum_documento() {
		return num_documento;
	}

	public void setNum_documento(int num_documento) {
		this.num_documento = num_documento;
	}

	public String getNombre_completo() {
		return nombre_completo;
	}

	public void setNombre_completo(String nombre_completo) {
		this.nombre_completo = nombre_completo;
	}

	public String getContraseña() {
		return contraseña;
	}

	public void setContraseña(String contraseña) {
		this.contraseña = contraseña;
	}

	public String getCorreo_electronico() {
		return correo_electronico;
	}

	public void setCorreo_electronico(String correo_electronico) {
		this.correo_electronico = correo_electronico;
	}

	public int getTelefono() {
		return telefono;
	}

	public void setTelefono(int telefono) {
		this.telefono = telefono;
	}

	public Rol getRol() {
		return rol;
	}

	public void setRol(Rol rol) {
		this.rol = rol;
	}

	
	@Override
	public String toString() {
		return "Usuario [id=" + id + ", num_documento=" + num_documento + ", nombre_completo=" + nombre_completo
				+ ", contraseña=" + contraseña + ", correo_electronico=" + correo_electronico + ", telefono=" + telefono
				+ ", rol=" + rol + "]";
	}
	

}
