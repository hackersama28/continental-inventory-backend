package com.inventorycontinental.entities;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Entity
public class Categoria implements Serializable {
	
	

	



	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_categoria_pk", nullable=false)
	private long id;
	
	@Column(name="nombre", nullable=false)
	private String nombre;
	
	@Column(name="codigo_categoria", nullable=false)
	private String CodigoCategoria;
	
	

	public Categoria(long id, String nombre, String codigoCategoria) {
		super();
		this.id = id;
		this.nombre = nombre;
		CodigoCategoria = codigoCategoria;
	}



	public Categoria() {
		super();
	}



	public long getId() {
		return id;
	}



	public void setId(long id) {
		this.id = id;
	}



	public String getNombre() {
		return nombre;
	}



	public void setNombre(String nombre) {
		this.nombre = nombre;
	}



	public String getCodigoCategoria() {
		return CodigoCategoria;
	}



	public void setCodigoCategoria(String codigoCategoria) {
		CodigoCategoria = codigoCategoria;
	}
	
	
	@Override
	public String toString() {
		return "Categoria [id=" + id + ", nombre=" + nombre + ", CodigoCategoria=" + CodigoCategoria + "]";
	}
	
	
	

}

