package com.inventorycontinental.services;



import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.inventorycontinental.dao.IMarcaDao;
import com.inventorycontinental.entities.Marca;
import com.inventorycontinental.servicesInterface.IMarcaServices;


@Service
public class MarcaServices implements IMarcaServices {
	
	@Autowired
	
	public IMarcaDao marcaDao;
	
	

	@Override
	public List<Marca> findAll() {
		// TODO Auto-generated method stub
		return (List<Marca>) marcaDao.findAll();
	}

	@Override
	public Marca create(Marca marca) {
		// TODO Auto-generated method stub
		return marcaDao.save(marca);
	}

	@Override
	public Marca update(long id, Marca marca) {
	
		Optional<Marca> mc = this.marcaDao.findById((long) id);
		if(mc.isPresent()) {
			marca.setId(mc.get().getId());
			return this.marcaDao.save(marca);}
		
		
		else{
	    	return new Marca(); 
	    }
			
			
	}



}
