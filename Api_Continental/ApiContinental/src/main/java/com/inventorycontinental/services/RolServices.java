package com.inventorycontinental.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.inventorycontinental.dao.IRolDao;
import com.inventorycontinental.entities.Rol;
import com.inventorycontinental.servicesInterface.IRolServices;

@Service
public class RolServices implements IRolServices{
	
	@Autowired
	private IRolDao rolDao;

	@Override
	public List<Rol> findAll() {
		return (List<Rol>) rolDao.findAll();
	}

	@Override
	public Rol create(Rol rol) {
		return rolDao.save(rol);
	}

	
	@Override
	public Rol update(long id, Rol rol) {
		Optional<Rol>r= this.rolDao.findById(id);
		if (r.isPresent()) {
		rol.setId(r.get().getId());	
		return this.rolDao.save(rol);
		}
		else {
			return new Rol();
		}
	}

	@Override
	public void delete(long id) {
		this.rolDao.deleteById(id);
		
	}
	
	
}


