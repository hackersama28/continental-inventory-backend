package com.inventorycontinental.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.inventorycontinental.dao.IUsuarioDao;
import com.inventorycontinental.entities.Usuario;
import com.inventorycontinental.servicesInterface.IUsuarioServices;

@Service
public class UsuarioServices implements IUsuarioServices {
	
	@Autowired
	private IUsuarioDao usuarioDao;
	@Override
	public List<Usuario> findAll() {
		return (List<Usuario>) usuarioDao.findAll();
	}

	@Override
	public Usuario create(Usuario usuario) {
		return usuarioDao.save(usuario);
	}

	@Override
	public Usuario update(long id, Usuario usuario) {
		Optional<Usuario> us = this.usuarioDao.findById(id);
		if(us.isPresent()) {
	    usuario.setId(us.get().getId());
		return this.usuarioDao.save(usuario);
		}
		else {
			return new Usuario();
		}
		
		
		
		
	}

	@Override
	public void delete(long id) {
		this.usuarioDao.deleteById(id);
		
	}

	@Override
	public Usuario findUsuarioByDocumentoyContraseña(int num_documento, String contraseña) {
		// TODO Auto-generated method stub
		return this.usuarioDao.findByDocumentoyContraseña(num_documento, contraseña);
	}

	
    
}


