package com.inventorycontinental.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.inventorycontinental.dao.IVentaDao;
import com.inventorycontinental.entities.Venta;
import com.inventorycontinental.servicesInterface.IVentaServices;

@Service
public class VentaServices implements IVentaServices  {
	
	
	@Autowired
	private IVentaDao ventaDao;
	

	@Override
	public List<Venta> findAll() {
		// TODO Auto-generated method stub
		return (List<Venta>) ventaDao.findAll();
	}

	@Override
	public Venta create(Venta venta) {
		// TODO Auto-generated method stub
		return ventaDao.save(venta);
	}

	@Override
	public Venta update(long id, Venta venta) {
		Optional<Venta> vn = this.ventaDao.findById((long) id);
		if(vn.isPresent()) {
			venta.setId(vn.get().getId());
			return this.ventaDao.save(venta);}
		
		
		else{
	    	return new Venta(); 
	    }
	}

}

