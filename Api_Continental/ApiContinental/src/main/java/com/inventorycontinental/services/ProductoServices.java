package com.inventorycontinental.services;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.inventorycontinental.dao.IProductoDao;
import com.inventorycontinental.entities.Producto;
import com.inventorycontinental.servicesInterface.IProductoServices;


@Service
public class ProductoServices implements IProductoServices {
	
	
	@Autowired
	 
	private IProductoDao productoDao;
	
	

	@Override
	public List<Producto> findAll() {
		// TODO Auto-generated method stub
		return (List<Producto>) productoDao.findAll();
	}

	@Override
	public Producto create(Producto producto) {
		// TODO Auto-generated method stub
		return productoDao.save(producto);
	}

	@Override
	public Producto update(long id, Producto producto) {
		Optional<Producto> pr = this.productoDao.findById((long) id);
		if(pr.isPresent()) {
			producto.setId(pr.get().getId());
			return this.productoDao.save(producto);}
		
		
		else{
	    	return new Producto(); 
	    }
	}

}

