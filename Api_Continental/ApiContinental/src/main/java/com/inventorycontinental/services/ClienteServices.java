package com.inventorycontinental.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.inventorycontinental.dao.IClienteDao;
import com.inventorycontinental.entities.Cliente;
import com.inventorycontinental.servicesInterface.IClienteServices;

@Service
public class ClienteServices implements IClienteServices {
	
	@Autowired
	
	private IClienteDao clienteDao;

	@Override
	public List<Cliente> findAll() {
		// TODO Auto-generated method stub
		return (List<Cliente>) clienteDao.findAll();
	}

	@Override
	public Cliente create(Cliente cliente) {
		// TODO Auto-generated method stub
		return clienteDao.save(cliente);
	}

	@Override
	public Cliente update(long id, Cliente cliente) {
		
		
		Optional<Cliente> cl = this.clienteDao.findById((long) id);
		if(cl.isPresent()) {
			cliente.setId(cl.get().getId());
			return this.clienteDao.save(cliente);}
		
		
		else{
	    	return new Cliente(); 
	    }
				
				
				
	}

}

