package com.inventorycontinental.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.inventorycontinental.dao.ICategoriaDao;
import com.inventorycontinental.entities.Categoria;

import com.inventorycontinental.servicesInterface.ICategoriaServices;


@Service
public class CategoriaServices implements ICategoriaServices {
	
	
	@Autowired
	
	private ICategoriaDao categoriaDao;
	

	@Override
	public List<Categoria> findAll() {
		// TODO Auto-generated method stub
		return (List<Categoria>) categoriaDao.findAll();
	}

	@Override
	public Categoria create(Categoria categoria) {
		// TODO Auto-generated method stub
		return categoriaDao.save(categoria);
	}

	@Override
	public Categoria update(long id, Categoria categoria) {
		Optional<Categoria> cat = this.categoriaDao.findById((long) id);
		if(cat.isPresent()) {
			categoria.setId(cat.get().getId());
			return this.categoriaDao.save(categoria);}
		
		
		else{
	    	return new Categoria(); 
	    }
		
	}

}

