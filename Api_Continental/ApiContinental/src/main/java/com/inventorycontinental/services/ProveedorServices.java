package com.inventorycontinental.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.inventorycontinental.dao.IProveedorDao;
import com.inventorycontinental.entities.Proveedor;
import com.inventorycontinental.servicesInterface.IProveedorServices;

@Service
public class ProveedorServices implements IProveedorServices {
	
	@Autowired

	private IProveedorDao proveedorDao;
	
	@Override
	public List<Proveedor> findAll() {
		// TODO Auto-generated method stub
		
		return (List<Proveedor>) proveedorDao.findAll();
	}

	@Override
	public Proveedor create(Proveedor proveedor) {
		// TODO Auto-generated method stub
		return proveedorDao.save(proveedor);
	}

	@Override
	public Proveedor update(long id, Proveedor proveedor) {
		// TODO Auto-generated method stub
		Optional<Proveedor> pr = this.proveedorDao.findById((long) id);
		if(pr.isPresent()) {
			proveedor.setId(pr.get().getId());
			return this.proveedorDao.save(proveedor);}
		
		
		else{
	    	return new Proveedor(); 
	    }
	}

}

