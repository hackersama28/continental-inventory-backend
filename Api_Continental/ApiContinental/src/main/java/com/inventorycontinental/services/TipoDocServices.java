package com.inventorycontinental.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.inventorycontinental.dao.ITipoDocDao;
import com.inventorycontinental.entities.TipoDoc;
import com.inventorycontinental.servicesInterface.ITipoDocServices;


@Service
public class TipoDocServices implements ITipoDocServices{
	
	
	@Autowired
	
	public ITipoDocDao tipo_docDao;
	

	@Override
	public List<TipoDoc> findAll() {
	
		return (List<TipoDoc>) tipo_docDao.findAll();
	}

	@Override
	public TipoDoc create(TipoDoc tipo_doc) {
		// TODO Auto-generated method stub
		return tipo_docDao.save(tipo_doc);
	}

	@Override
	public TipoDoc update(long id, TipoDoc tipo_doc) {
		
		
		Optional<TipoDoc> tpd = this.tipo_docDao.findById((long) id);
		if(tpd.isPresent()) {
			tipo_doc.setId(tpd.get().getId());
			return this.tipo_docDao.save(tipo_doc);}
		
		
		else{
	    	return new TipoDoc(); 
	    }
			
	}

}
