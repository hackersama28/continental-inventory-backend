package com.inventorycontinental.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.inventorycontinental.dao.ICompraDao;
import com.inventorycontinental.entities.Compra;
import com.inventorycontinental.servicesInterface.ICompraServices;

@Service
public class CompraServices implements ICompraServices {
	
@Autowired
	
	private ICompraDao compraDao;

	@Override
	public List<Compra> findAll() {
		// TODO Auto-generated method stub
		return (List<Compra>) compraDao.findAll();
	}

	@Override
	public Compra create(Compra compra) {
		// TODO Auto-generated method stub
		return compraDao.save(compra);
	}

	@Override
	public Compra update(long id, Compra compra) {
		
		
		Optional<Compra> cm = this.compraDao.findById((long) id);
		if(cm.isPresent()) {
			compra.setId(cm.get().getId());
			return this.compraDao.save(compra);}
		
		
		else{
	    	return new Compra(); 
	    }
	}			

}
