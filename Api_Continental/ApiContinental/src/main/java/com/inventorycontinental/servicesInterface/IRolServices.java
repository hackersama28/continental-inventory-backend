package com.inventorycontinental.servicesInterface;

import java.util.List;

import com.inventorycontinental.entities.Rol;

public interface IRolServices {
	
	public List<Rol>findAll();
	
	public Rol create (Rol rol);

	public Rol update(long id, Rol rol);

	public void delete(long id);



}

