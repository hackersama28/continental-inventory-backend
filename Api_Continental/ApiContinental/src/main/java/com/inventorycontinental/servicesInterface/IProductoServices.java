package com.inventorycontinental.servicesInterface;

import java.util.List;
import com.inventorycontinental.entities.Producto;




public interface IProductoServices {

	public List <Producto> findAll();
	
	public Producto create (Producto  producto);

	public Producto update(long id, Producto producto);
	
	
}
