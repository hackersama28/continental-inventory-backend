package com.inventorycontinental.servicesInterface;

import java.util.List;

import com.inventorycontinental.entities.Usuario;

public interface IUsuarioServices {
	
	public List<Usuario>findAll();
	
	public Usuario create(Usuario usuario);

	public Usuario update(long id, Usuario usuario);

	public void delete(long id);

	public Usuario findUsuarioByDocumentoyContraseña(int num_documento, String contraseña);



	

}


