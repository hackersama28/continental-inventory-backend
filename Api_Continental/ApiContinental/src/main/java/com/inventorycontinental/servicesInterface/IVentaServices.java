package com.inventorycontinental.servicesInterface;

import java.util.List;

import com.inventorycontinental.entities.Venta;

public interface IVentaServices {
	
	public List <Venta> findAll();
	
	public Venta create (Venta  venta);

	public Venta update(long id, Venta venta);

}
