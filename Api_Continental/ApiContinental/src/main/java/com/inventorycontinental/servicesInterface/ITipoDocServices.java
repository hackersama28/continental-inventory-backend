package com.inventorycontinental.servicesInterface;

import java.util.List;

import com.inventorycontinental.entities.TipoDoc;



public interface ITipoDocServices {

	
	public List <TipoDoc> findAll();
	
    public TipoDoc create (TipoDoc  tipo_doc);

	public TipoDoc update(long id, TipoDoc tipo_doc);

	
}


