package com.inventorycontinental.servicesInterface;

import java.util.List;

import com.inventorycontinental.entities.Categoria;


		public interface ICategoriaServices {
			
		    public List <Categoria> findAll();
			
			public Categoria create (Categoria  categoria);

			public Categoria update(long id, Categoria categoria);
		
		}
