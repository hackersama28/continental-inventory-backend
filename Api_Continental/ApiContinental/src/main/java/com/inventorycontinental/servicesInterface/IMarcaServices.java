package com.inventorycontinental.servicesInterface;

import java.util.List;

import com.inventorycontinental.entities.Marca;



public interface IMarcaServices {
	
	
		public List <Marca> findAll();
			
	    public Marca create (Marca  marca);

		public Marca update(long id, Marca marca);

		

}
