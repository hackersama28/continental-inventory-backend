package com.inventorycontinental.servicesInterface;

import java.util.List;

import com.inventorycontinental.entities.Cliente;



public interface IClienteServices {
	
	 public List <Cliente> findAll();
		
		public Cliente create (Cliente  cliente);

		public Cliente update(long id, Cliente cliente);

}

