package com.inventorycontinental.servicesInterface;

import java.util.List;

import com.inventorycontinental.entities.Proveedor;



public interface IProveedorServices {
	
	
	 public List <Proveedor> findAll();
		
		public Proveedor create (Proveedor  proveedor);

		public Proveedor update(long id, Proveedor proveedor);

}

