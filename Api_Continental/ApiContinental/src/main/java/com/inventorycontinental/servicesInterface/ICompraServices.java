package com.inventorycontinental.servicesInterface;

import java.util.List;

import com.inventorycontinental.entities.Compra;

public interface ICompraServices {
	
	 public List <Compra> findAll();
		
		public Compra create (Compra  compra);

		public Compra update(long id, Compra compra);

}
