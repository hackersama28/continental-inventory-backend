package com.inventorycontinental.dao;

import org.springframework.data.repository.CrudRepository;

import com.inventorycontinental.entities.Producto;

public interface IProductoDao extends CrudRepository <Producto, Long>{

}

