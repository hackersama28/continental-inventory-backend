package com.inventorycontinental.dao;

import org.springframework.data.repository.CrudRepository;

import com.inventorycontinental.entities.Proveedor;



public interface IProveedorDao extends CrudRepository <Proveedor, Long> {

}
