package com.inventorycontinental.dao;

import org.springframework.data.repository.CrudRepository;

import com.inventorycontinental.entities.Compra;

public interface ICompraDao extends CrudRepository <Compra, Long> {

}
