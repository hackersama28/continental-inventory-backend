package com.inventorycontinental.dao;

import org.springframework.data.repository.CrudRepository;

import com.inventorycontinental.entities.Marca;

public interface IMarcaDao extends CrudRepository <Marca , Long>{

}

