package com.inventorycontinental.dao;

import org.springframework.data.repository.CrudRepository;

import com.inventorycontinental.entities.Rol;


public interface IRolDao extends CrudRepository<Rol,Long>{

}


