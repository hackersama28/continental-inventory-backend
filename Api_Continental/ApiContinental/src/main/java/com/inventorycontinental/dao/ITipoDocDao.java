package com.inventorycontinental.dao;

import org.springframework.data.repository.CrudRepository;

import com.inventorycontinental.entities.TipoDoc;



public interface ITipoDocDao extends CrudRepository <TipoDoc , Long> {
	
	
}
