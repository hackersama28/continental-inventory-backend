package com.inventorycontinental.dao;

import org.springframework.data.repository.CrudRepository;

import com.inventorycontinental.entities.Usuario;

public interface IUsuarioDao extends CrudRepository<Usuario,Long> {
	
	public Usuario findByDocumentoyContraseña(int num_documento, String contraseña);

}


