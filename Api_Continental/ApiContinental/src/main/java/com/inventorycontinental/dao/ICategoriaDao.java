package com.inventorycontinental.dao;

import org.springframework.data.repository.CrudRepository;

import com.inventorycontinental.entities.Categoria;

public interface ICategoriaDao extends CrudRepository <Categoria, Long> {

}

